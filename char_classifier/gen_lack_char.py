import sys
sys.path.append('..')

from fake_library import gen_data
from embed_train import char_size
import cv2, numpy as np, os, shutil
cv2.imwrite=lambda path,img: cv2.imencode('.jpg', img)[1].tofile(path)

fonts=[
    'art.ttf', 'Calligraphy.ttf', 'Cartoon.ttf', 'CP Font.ttf', 'Engineering.ttf', 'FZZJ-ZGXTJW.TTF',
    'General_Art.ttf', 'Graffiti.ttf', 'Hand_painted.ttf',
    'PangMenZhengDaoZhenGuiKaiTi-2.ttf', 'RampartOne-Regular.ttf', 'RocknRollOne-Regular.ttf', 'Round.otf',
    'ShigotoMemogaki-Regular-1-01.ttf', 'Stamp.ttf', 'WCL-03.ttf', 'wt028.ttf', '喪沄瑄閡嘔矓极.ttf',
    '宅在家自動筆20200605.ttf', '衡山毛筆.ttf'
]
bgs=os.listdir('background')

for c in range(19968, 40917+1):
    char=chr(c)
    if not os.path.exists(f'data/{char}'):
        os.mkdir(f'data/{char}')
    for ind,font in enumerate(fonts):
        img=cv2.imread(f'background/{np.random.choice(bgs, 1)[0]}')
        pos_a=np.random.randint(0, img.shape[0]-char_size)
        pos_b=np.random.randint(0, img.shape[1]-char_size)
        img=img[pos_a:pos_a+char_size, pos_b:pos_b+char_size, :]
        
        char_pack=[{
            'char': chr(c),
            'size': char_size,
            'font': np.random.choice(fonts, 1)[0],
            'position': (char_size//2, char_size//2),
            'color': (np.random.randint(0, 255), np.random.randint(0, 255), np.random.randint(0, 255))
        }]
        result, boxes, success=gen_data(img, char_pack, font_path='../fake_library/fonts/')
        if False in success:
            continue
        else:
            cv2.imwrite(f'data/{char}/gen_{ind}.jpg', result)
            print(f'data/{char}/gen_{ind}.jpg')


for folder in os.listdir('data'):
    if len(os.listdir(f'data/{folder}'))==0:
        print(folder)
        shutil.rmtree(f'data/{folder}')


''' create npy for embed_train.py '''
cv2.imread=lambda path: cv2.imdecode(np.fromfile(path, dtype=np.uint8), cv2.IMREAD_COLOR)
x=[]
y=[]
for cla,label in enumerate(os.listdir('data')):
    for file in os.listdir(f'data/{label}'):
        img=cv2.imread(f'data/{label}/{file}')
        img=cv2.resize(img, (char_size, char_size))
        x.append(img)
        y.append(cla)
x=np.array(x)
y=np.array(y)
print(x.shape, x.dtype)
print(y.shape, y.dtype)

np.save('data_x', x)
np.save('data_y', y)

