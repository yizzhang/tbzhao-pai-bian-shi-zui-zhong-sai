from embed_train import char_size, resmodel
import numpy as np, os, cv2

x=np.load('data_x.npy')

model=resmodel()
model.load_weights('model_embed.h5')
embed=model.predict(x.astype('float32')/255.0)
print(embed.shape, embed.dtype)

np.save('data_embed', embed)

# np.load('model_add_more/data_y.npy').shape
