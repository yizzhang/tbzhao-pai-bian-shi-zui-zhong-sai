import os, re, cv2, json, numpy as np, shutil

cv2.imwrite=lambda path,img: cv2.imencode('.jpg', img)[1].tofile(path)

try:
    shutil.rmtree('data')
except:
    pass
os.mkdir('data')

for file in os.listdir('../__datasets__/train/img'):
    name='.'.join(re.split('[.]', file)[:-1])
    img=cv2.imread('../__datasets__/train/img/'+file)
    boxes=json.loads(open(f'../__datasets__/train/json/{name}.json', 'r', encoding='utf-8').read())['shapes']
    for ind, box in enumerate(boxes):
        if len(box['label'])==1:
            if box["label"].isalnum():
                min_x=np.min([i[0] for i in box['points']])
                min_x=0 if min_x<0 else min_x
                min_y=np.min([i[1] for i in box['points']])
                min_y=0 if min_y<0 else min_y
                max_x=np.max([i[0] for i in box['points']])
                max_x=img.shape[1]-1 if max_x>=img.shape[1] else max_x
                max_y=np.max([i[1] for i in box['points']])
                max_y=img.shape[0]-1 if max_y>=img.shape[0] else max_y
                crop=img[min_y:max_y+1, min_x:max_x+1]
                if not os.path.exists(f'data/{box["label"]}'):
                    os.mkdir(f'data/{box["label"]}')
                cv2.imwrite(f'data/{box["label"]}/{name}_{ind}.jpg', crop)
                print(name, box["label"], ind)

