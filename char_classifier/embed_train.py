from tensorflow.keras.layers import (
    Dense, GlobalAveragePooling2D, Lambda
)
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.models import Model
from tensorflow.keras import backend as K
from tensorflow.keras.optimizers.schedules import LearningRateSchedule
import tensorflow as tf
import numpy as np, os

char_size=64

def resmodel():
    from classification_models.tfkeras import Classifiers
    ResNet18, _ = Classifiers.get('resnet18')
    base_model = ResNet18(
        input_shape=(char_size,char_size,3),
        weights='imagenet',
        include_top=False
    )
    x = GlobalAveragePooling2D()(base_model.output)
    output = Dense(128, activation='linear')(x)
    output=Lambda(lambda  x: K.l2_normalize(x,axis=1))(output)
    model = Model(
        inputs=[base_model.input],
        outputs=[output]
    )
    return model


class TrainGenerator:
    def __init__(self, pick_types=6, pos_count=100):
        self.pick_types=pick_types
        self.pos_count=pos_count
        self.neg_count=int(pos_count/(pick_types-1))
        
        self.x=np.load('data_x.npy')
        self.y=np.load('data_y.npy')
        self.labels=np.array(os.listdir('data'))
        print(self.x.shape, self.y.shape, self.labels.shape)
    
    def generator_by_types(self, type_per_batch=720, images_per_type=5):
        while 1:
            types = np.random.choice(self.labels, type_per_batch, replace=False)
            sample_ind = np.where(np.isin(self.labels, types))[0]
            n_choices = {
                i:np.random.choice(
                    len(self.x[np.where(self.y==i)]),
                    images_per_type,
                    replace=True
                )
                for i in sample_ind
            }
            x = np.concatenate([self.x[np.where(self.y==i)][n_choices[i]] for i in sample_ind], axis=0)
            y = np.concatenate([np.full((images_per_type,), i) for i in sample_ind], axis=0)
            x=x.astype('float32')/255.0
            y=y.astype('float32')
            yield x, y

def distances(embeda, embedb):
    dist = np.linalg.norm(embeda-embedb)
    return dist



if __name__=='__main__':
    from triplet_loss import batch_hard_triplet_loss
    
    name='model_embed'
    type_per_batch=128
    steps_per_epoch=500
    model = resmodel()
    loss_fn=batch_hard_triplet_loss
    
    class MyLRSchedule(LearningRateSchedule):
        def __init__(self, lr, steps_per_epoch):
            self.initial_learning_rate = lr
            self.steps_per_epoch=steps_per_epoch
        
        def __call__(self, step):
            epoch=step//self.steps_per_epoch
            return self.initial_learning_rate*(0.98**(epoch//4))
    
    optimizer = Adam(learning_rate=1e-4) # 0.1 0.01 0.001 0.0001
    
    from tqdm import tqdm
    TG=TrainGenerator()
    generator=TG.generator_by_types(type_per_batch=type_per_batch)
    best_loss=np.inf
    for epoch in range(1, 999999999):
        losses=[]
        for step in tqdm(range(1, steps_per_epoch+1), desc=f'Epoch {epoch}'):
            x,y=next(generator)
            with tf.GradientTape() as tape:
                embeds=model(x, training=True)
                loss=loss_fn(y, embeds)
            grads=tape.gradient(loss, model.trainable_variables)
            optimizer.apply_gradients(zip(grads, model.trainable_variables))
            losses.append(loss)
        loss=tf.reduce_mean(losses).numpy()
        print(loss)
        if loss<best_loss:
            best_loss=loss
            model.save_weights(f'{name}.h5')
            print('save weights')
    

