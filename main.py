import cv2, numpy as np, os, re, shutil
cv2.imwrite=lambda path,img: cv2.imencode('.jpg', img)[1].tofile(path)

net=cv2.dnn_DetectionModel(
    'yolov4-chineseword.cfg',
    'backup/yolov4-chineseword_last.weights'
)
net.setInputScale(1.0 / 255.0)
net.setInputSwapRB(True)
net.setInputSize(608, 608)

def detect(d):
    file, path, fit=d # default: True, False
    img=cv2.imread(path+file)
    if fit:
        axis0_pad=32-(img.shape[0]%32)
        axis1_pad=32-(img.shape[1]%32)
        axis0_pad=0 if axis0_pad==32 else axis0_pad
        axis1_pad=0 if axis1_pad==32 else axis1_pad
        img=np.pad(img, ((axis0_pad,0),(axis1_pad,0),(0,0)))
        net.setInputSize(img.shape[1], img.shape[0])
    classes, confidences, boxes = net.detect(img, confThreshold=0.3, nmsThreshold=0.3)
    if fit:
        boxes[:,0]-=axis1_pad
        boxes[:,1]-=axis0_pad
        oob=np.where( boxes[:,0]<0 )
        reduce=0-boxes[:,0][oob]
        boxes[:,2][oob]-=reduce
        boxes[:,0][oob]=0
        oob=np.where( boxes[:,1]<0 )
        reduce=0-boxes[:,1][oob]
        boxes[:,3][oob]-=reduce
        boxes[:,1][oob]=0
    print(file)
    return boxes

def classify(path, pool_outputs, char_size, model):
    char_imgs=[]
    for file, boxes in zip(os.listdir(path), pool_outputs):
        img=cv2.imread(f'{path}{file}')
        for box in boxes:
            char_img=img[box[1]:box[1]+box[3], box[0]:box[0]+box[2]].copy()
            char_img=cv2.resize(char_img, (char_size,char_size))
            char_imgs.append(char_img)
    
    char_imgs=np.array(char_imgs)
    char_imgs=char_imgs.astype('float32')/255.0
    embeds=model.predict(char_imgs)
    return embeds

def pack_embeds(pool_outputs, embeds):
    result=[]
    for boxes in pool_outputs:
        result.append(embeds[:len(boxes)])
        embeds=embeds[len(boxes):]
        print(len(embeds))
    return result

def embed_to_labels(embed, data_embed, label_names, data_y):
    embed=np.array([embed])
    dists=np.linalg.norm(embed-data_embed, axis=-1)
    dists_ind=np.argmin(dists, -1)
    
    pred=label_names[ data_y[dists_ind] ]
    pred='#' if pred=='other' else pred
    return pred

from PIL import ImageFont, ImageDraw, Image
def visual(img, boxes, labels, text=True):
    line_size=np.clip(np.round(np.min(img.shape[:2])/220), 1, 10).astype('int')
    pred_mask=np.zeros(img.shape, dtype='uint8')
    for box in boxes:
        cv2.rectangle(pred_mask, (box[0],box[1]), (box[0]+box[2],box[1]+box[3]), (0,255,0), thickness=line_size//2)
    pred_mask=Image.fromarray(pred_mask)
    draw = ImageDraw.Draw(pred_mask)
    if text:
        for box, label in zip(boxes, labels):
            font = ImageFont.truetype('simfang.ttf', np.min(box[2:4]))
            char_size=font.getsize(label)
            draw_x=box[0]+(box[2]//2)-(char_size[0]//2)
            draw_y=box[1]+(box[3]//2)-(char_size[1]//2)
            draw.text(xy=(draw_x, draw_y), text=label, fill="white", font=font)
    pred_mask=np.array(pred_mask)
    return pred_mask

def result(d):
    file, path, boxes, embeds, data_embed, label_names, data_y=d
    labels=[]
    for embed in embeds:
        labels.append(embed_to_labels(embed, data_embed, label_names, data_y))
    csv=''
    name='.'.join(re.split('[.]', file)[:-1])
    img=cv2.imread(path+file)
    for box,label in zip(boxes, labels):
        if label=='#':
            continue
        x0=box[0]
        y0=box[1]
        x1=box[0]+box[2]
        y1=box[1]
        x2=box[0]+box[2]
        y2=box[1]+box[3]
        x3=box[0]
        y3=box[1]+box[3]
        csv+=f'{name},{",".join(np.array([x0,y0,x1,y1,x2,y2,x3,y3]).astype("str"))},{label}\n'
    pred=visual(img, boxes, labels, text=True)
    cv2.imwrite(f'__predict__/{name}.jpg', img)
    cv2.imwrite(f'__predict__/{name}_pred.jpg', pred)
    print(name)
    return csv

from multiprocessing import Pool

if __name__=='__main__':
    try:
        shutil.rmtree('__predict__')
    except:
        pass
    os.mkdir('__predict__')
    
    path='__datasets__/private/'
    
    import time
    t=time.time()
    pool = Pool(os.cpu_count())
    pool_outputs = pool.map(detect, [(file,path,True) for file in os.listdir(path)])
    
    from char_classifier.embed_train import resmodel, char_size
    label_names=np.array(os.listdir('char_classifier/data'))
    model=resmodel()
    model.load_weights('char_classifier/model_embed.h5')
    data_embed=np.load('char_classifier/data_embed.npy')
    data_y=np.load('char_classifier/data_y.npy')
    
    embeds=classify(path, pool_outputs, char_size, model)
    embed_outputs=pack_embeds(pool_outputs, embeds)
    
    csv=pool.map(
        result,
        [
            (file, path, boxes, embeds, data_embed, label_names, data_y)
            for (file, boxes, embeds) in zip(
                    os.listdir(path), pool_outputs, embed_outputs
                )]
    )
    csv=''.join(csv)
    open('submit.csv', 'w', encoding='utf-8').write(csv)
    
    print(time.time()-t)
    
