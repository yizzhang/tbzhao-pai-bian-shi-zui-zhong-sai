import json, os, shutil, cv2, numpy as np, re
from os import listdir
from os.path import isfile, isdir, join


mypath ="__datasets__/train" #你的目錄

try:
    shutil.rmtree('data')
except:
    pass
os.mkdir('data')

for f in listdir(f'{mypath}/json'):
    jsonfile=open(f'{mypath}/json/{f}',encoding='utf-8').read()
    data=json.loads(jsonfile)
    picname=f[:-5]
    img=cv2.imread(f'{mypath}/img/{picname}.jpg')
    height, width = data['imageHeight'],data['imageWidth']
    annotation_str=''
    g_id=False
    
    for i in range(len(data['shapes'])):
        if data['shapes'][i]['group_id']==2 or data['shapes'][i]['group_id']==3 :
            g_id=True
    
    
    
    for i in range(len(data['shapes'])):
        if g_id==True:
            break
        if (len(data['shapes'][i]['label'])==1):
            x0=data['shapes'][i]['points'][0][0]
            y0=data['shapes'][i]['points'][0][1]
            x1=data['shapes'][i]['points'][1][0]
            y1=data['shapes'][i]['points'][1][1]
            x2=data['shapes'][i]['points'][2][0]
            y2=data['shapes'][i]['points'][2][1]
            x3=data['shapes'][i]['points'][3][0]
            y3=data['shapes'][i]['points'][3][1]
            xmin=min(x0,x1,x2,x3)
            xmax=max(x0,x1,x2,x3)
            ymin=min(y0,y1,y2,y3)
            ymax=max(y0,y1,y2,y3)
            center_x = (xmax + xmin) / 2 / int(width)
            center_y = (ymax + ymin) / 2 / int(height)
            box_width = (xmax - xmin) / int(width)
            box_height = (ymax - ymin) / int(height)
            annotation_str+=f'0 {center_x} {center_y} {box_width} {box_height}\n'
        open(f'data/{picname}.txt','w',encoding='utf-8').write(annotation_str)
        cv2.imwrite(f'data/{picname}.jpg', img)
        


