import cv2, numpy as np

def random_skew(img):
    if np.random.rand()>0.5:
        h,w=img.shape[:2]
        src=np.array([
            [0, 0],
            [w - 1, 0],
            [0, h - 1],
            [w - 1, h - 1]
        ]).astype(np.float32)
        dst=np.array([
            [np.random.randint(0, int(w*0.40)), np.random.randint(0, int(h*0.2))],
            [np.random.randint(int(w*0.60), w), np.random.randint(0, int(h*0.2))],
            [np.random.randint(0, int(w*0.40)), np.random.randint(int(h*0.8), h)],
            [np.random.randint(int(w*0.60), w), np.random.randint(int(h*0.8), h)]
        ]).astype(np.float32)
        warp_mat = cv2.getPerspectiveTransform(src, dst)
        result = cv2.warpPerspective(
            img,
            warp_mat,
            (w, h),
            flags=cv2.INTER_CUBIC,
            borderMode=cv2.BORDER_REPLICATE
        )
        mask = cv2.warpPerspective(
            np.zeros(img.shape[:2], dtype='uint8'),
            warp_mat,
            (w, h),
            flags=cv2.INTER_NEAREST,
            borderMode=cv2.BORDER_CONSTANT,
            borderValue=255
        )
        y,x=np.where(mask==0)
        min_x=np.min(x)
        max_x=np.max(x)
        min_y=np.min(y)
        max_y=np.max(y)
        img=result[min_y:max_y+1, min_x:max_x+1]
    return img
