import numpy as np, cv2, os
from PIL import ImageFont, ImageDraw, Image
from fontTools.ttLib import TTFont
import logging
logger = logging.getLogger('fontTools').setLevel(logging.ERROR)

def draw_font(fontpath, char, font_size, color):
    '''
    # fontpath='fake_library/fonts/Engineering.ttf'
    fontpath='fake_library/fonts/art.ttf'
    char='C'
    font_size=40
    color=(128,50,0)
    '''
    font = ImageFont.truetype(fontpath, font_size)
    width, height=font.getsize(char[-1])
    
    Achannel=np.zeros((height,width), dtype='uint8')
    Achannel=Image.fromarray(Achannel)
    draw = ImageDraw.Draw(Achannel)
    draw.text(xy=(0, 0), text=char[-1], fill="#FFFFFF", font=font)
    Achannel=np.array(Achannel)
    Achannel=np.expand_dims(Achannel, axis=-1)
    
    BGR=np.zeros((height,width,3), dtype='uint8')
    BGR=Image.fromarray(BGR)
    draw = ImageDraw.Draw(BGR)
    draw.text(xy=(0, 0), text=char[-1], fill='#%02x%02x%02x'%color, font=font)
    BGR=np.array(BGR)
    
    result=np.concatenate((BGR, Achannel), axis=-1)
    return result

def blend_transparent(char_img, img):
    overlay_img = char_img[:,:,:3]
    overlay_mask = char_img[:,:,3:]
    background_mask = 255-overlay_mask    
    overlay_mask = cv2.cvtColor(overlay_mask, cv2.COLOR_GRAY2BGR)
    background_mask = cv2.cvtColor(background_mask, cv2.COLOR_GRAY2BGR)
    img_part = (img * (1 / 255.0)) * (background_mask * (1 / 255.0))
    char_part = (overlay_img * (1 / 255.0)) * (overlay_mask * (1 / 255.0))
    return cv2.addWeighted(img_part, 255.0, char_part, 255.0, 0.0).astype('uint8')

def has_glyph(font, glyph):
    for table in font['cmap'].tables:
        if ord(glyph) in table.cmap.keys():
            return True
    return False

def gen_data(img, char_pack, font_path='fake_library/fonts/'):
    boxes=[]
    success=[]
    char_holder=np.zeros(img.shape[:-1]+(4,), dtype='uint8')
    for char_config in char_pack:
        success.append(False)
        if not has_glyph(TTFont(f"{font_path}{char_config['font']}"), char_config['char']):
            print('have not glyph')
            continue
        char_img=draw_font(f"{font_path}{char_config['font']}", char_config['char'], char_config['size'], char_config['color'])
        if not np.isin(255, char_img):
            print('not exist 255')
            continue
        tl_axis0=char_config['position'][1]-(char_img.shape[0]//2)
        tl_axis1=char_config['position'][0]-(char_img.shape[1]//2)
        if np.sum(char_holder[
                tl_axis0:tl_axis0+char_img.shape[0],
                tl_axis1:tl_axis1+char_img.shape[1]
        ])==0:
            try:
                char_holder[
                    tl_axis0:tl_axis0+char_img.shape[0],
                    tl_axis1:tl_axis1+char_img.shape[1]
                ]=char_img
                boxes.append([tl_axis1,tl_axis0,char_img.shape[1],char_img.shape[0]])
                success[-1]=True
            except:
                pass
    result=blend_transparent(char_holder, img)
    return result, boxes, success


if __name__=='__main__':
    img=cv2.imread('__datasets__/street_scene/bg (1).jpg')
    char_pack=[]
    fonts=[
        'art.ttf', 'Calligraphy.ttf', 'Cartoon.ttf', 'CP Font.ttf', 'Engineering.ttf', 'FZZJ-ZGXTJW.TTF',
        'General_Art.ttf', 'Graffiti.ttf', 'Hand_painted.ttf', 'KouzanBrushFontSousyo.ttf',
        'PangMenZhengDaoZhenGuiKaiTi-2.ttf', 'RampartOne-Regular.ttf', 'RocknRollOne-Regular.ttf', 'Round.otf',
        'ShigotoMemogaki-Regular-1-01.ttf', 'Stamp.ttf', 'WCL-03.ttf', 'wt028.ttf', '喪沄瑄閡嘔矓极.ttf',
        '宅在家自動筆20200605.ttf', '衡山毛筆.ttf', '迷你繁篆书.ttf'
    ]
    for i in range(30):
        char_config={
            'char': chr(np.random.randint(19968, 40917+1)),
            'size': np.random.randint(30, 80),
            'font': np.random.choice(fonts, 1)[0],
            'position': (np.random.randint(30, img.shape[1]-50), np.random.randint(30, img.shape[0]-50)),
            'color': (np.random.randint(0, 255), np.random.randint(0, 255), np.random.randint(0, 255))
        }
        char_pack.append(char_config)
    result, boxes=gen_data(img, char_pack)
    cv2.imwrite('test.jpg', result)



