import json, os, shutil, cv2, numpy as np, re
from os import listdir
from os.path import isfile, isdir, join


mypath ="__datasets__/RECTS" #你的目錄

for f in listdir(f'{mypath}/gt'):
    jsonfile=open(f'{mypath}/gt/{f}',encoding='utf-8').read()
    data=json.loads(jsonfile)
    picname=f[:-5]
    imgpath=f'{mypath}/img/{picname}.jpg'
    img=cv2.imread(imgpath)
    height, width = img.shape[:2]
    chars=data['chars']
    annotation_str=''
    if len(chars)==0:
        print(0, picname)
        continue
    for i in range(len(chars)):
        if not chars[i]['transcription'] in "!@#$%^&*()_-+={}[] ,.。~\\/\"'<>:;|、，‵！＠＃＄％︿＆＊（）＿＋｛｝｜：＂＜＞？～［］＼；’，．／－＝" :
            #not chars[i]['transcription'].encode().isalpha() and
            #not chars[i]['transcription'].encode().isalnum() and
            #x0,y0,x1,y1,x2,y2,x3,y3,x4,y4=chars[i]['points']
            x0=chars[i]['points'][0]
            y0=chars[i]['points'][1]
            x1=chars[i]['points'][2]
            y1=chars[i]['points'][3]
            x2=chars[i]['points'][4]
            y2=chars[i]['points'][5]
            x3=chars[i]['points'][6]
            y3=chars[i]['points'][7]
            xmin=min(x0,x1,x2,x3)
            xmax=max(x0,x1,x2,x3)
            ymin=min(y0,y1,y2,y3)
            ymax=max(y0,y1,y2,y3)
            
            center_x = (xmax + xmin) / 2 / width
            center_y = (ymax + ymin) / 2 / height
            box_width = (xmax - xmin) / width
            box_height = (ymax - ymin) / height
            annotation_str+=f'0 {center_x} {center_y} {box_width} {box_height}\n'
    open(f'data/{picname}.txt','w',encoding='utf-8').write(annotation_str)
    cv2.imwrite(f'data/{picname}.jpg', img)

train_list=[f'data/{i}' for i in os.listdir('data') if i[-3:]=='jpg']
train_list='\n'.join(train_list)
train_list+='\n'
open('train.txt','w',encoding='utf-8').write(train_list)
open('data/classes.txt', 'w').write('char\n')




