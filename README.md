# 環境
- Python 3
- [Darknet](https://github.com/AlexeyAB/darknet)


# 套件
- classification-models==0.1
- fontTools==4.28.5
- image-classifiers==1.0.0
- numpy==1.19.5
- opencv_python==4.4.0.46
- Pillow==8.4.0
- tensorflow_gpu==2.5.0rc0
- tqdm==4.60.0

# 執行流程
##### 物件偵測
1. 從競賽資料集產生yolo訓練資料，用以訓練YOLOv4模型
   ```shell
   python gen_yolo_data_1.py
   ```
2. 從RECTS資料集產生yolo訓練資料，用以訓練YOLOv4模型
   ```shell
   python gen_yolo_data_2.py
   ```
3. 開始訓練YOLOv4模型
   ```shell
   train.bat
   ```

##### 字元辨識
1. 將目錄切換至char_classifier目錄下
2. 產生data目錄，存放官方train資料中的字元影像，完成後需將裡面的英文及數字目錄移除
   ```shell
   python gen_data.py
   ```
3. 產生所有中文字元的生成影像，以補足訓練資料缺少的字元
   ```shell
   python gen_lack_char.py
   ```
4. 訓練影像編碼模型
   ```shell
   python embed_train.py
   ```
5. 產生編碼資料
   ```shell
   python embed_npy_creator.py
   ```

##### 預測
1. 使用存放於backup目錄中的物件偵測模型及char_classifier下的字元辨識模型和編碼資料，預測______datasets_____/private中的影像，並產生submit.csv
   ```shell
   python main.py
   ```

